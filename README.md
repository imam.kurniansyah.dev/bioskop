# ERD Image
![ERD](ERD.png)

# See API Documentation:
https://cari-tiket-nonton.herokuapp.com/api-documentation.html

# Export PostMan collection
[Postman Collection](Challenge 4.postman_collection.json)
