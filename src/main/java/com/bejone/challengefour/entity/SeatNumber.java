package com.bejone.challengefour.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class SeatNumber implements Serializable {
    private String scheduleId;
    private String userId;

    public SeatNumber() {
    }

    public SeatNumber(String scheduleId, String userId) {
        this.scheduleId = scheduleId;
        this.userId = userId;
    }
}
