package com.bejone.challengefour.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "seats")
public class Seat {
    @Column(name = "studio_name")
    private String studioName;

    @EmbeddedId
    @Column(name = "seat_number")
    private SeatNumber seatNumber;

    @Column(name = "is_available")
    private int isAvailable;
}
