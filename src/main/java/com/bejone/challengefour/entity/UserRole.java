package com.bejone.challengefour.entity;

public enum UserRole {
    ADMIN, USER
}
