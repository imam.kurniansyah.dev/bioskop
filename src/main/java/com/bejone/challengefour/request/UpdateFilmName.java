package com.bejone.challengefour.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class UpdateFilmName {
    @JsonProperty("id")
    private long id;

    @JsonProperty("new_name")
    private String newName;
}
