package com.bejone.challengefour.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class UpdateUser {
    private long id;
    private String email;
    private String password;
    @JsonProperty("user_name")
    private String userName;
}
