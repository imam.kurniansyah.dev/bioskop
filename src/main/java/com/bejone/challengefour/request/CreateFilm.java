package com.bejone.challengefour.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class CreateFilm {
    @JsonProperty("name")
    private String name;

    @JsonProperty("is_playing")
    private int isPlaying;
}
