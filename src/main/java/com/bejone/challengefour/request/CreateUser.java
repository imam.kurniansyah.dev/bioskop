package com.bejone.challengefour.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class CreateUser {
    @JsonProperty("user_name")
    private String userName;
    private String email;
    private String password;
    @JsonProperty("role_id")
    private int roleId;
}
