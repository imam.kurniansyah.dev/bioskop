package com.bejone.challengefour.model;

public interface FilmSchedule {
    String getName();
    int getPlaying();
    String getDate();
    String getStart();
    String getEnd();
}
