package com.bejone.challengefour.model;

public interface TicketReservation {
    String getFilm();
    String getUser();
    String getStudio();
    int getSeat();
    String getDate();
    String getStart();
    String getEnd();
}
