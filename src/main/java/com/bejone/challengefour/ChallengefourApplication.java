package com.bejone.challengefour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengefourApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengefourApplication.class, args);
	}

}
