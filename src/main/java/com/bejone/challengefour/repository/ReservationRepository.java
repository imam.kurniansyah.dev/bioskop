package com.bejone.challengefour.repository;

import com.bejone.challengefour.entity.Reservation;
import com.bejone.challengefour.model.TicketReservation;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Modifying
    @Query(value = "SELECT film.name as film, user.username as user, seat.studio_name as studio, seat.seat_number as seat, schedule.date as date, schedule.start as start, schedule.end as end \n"
            + "FROM films film "
            + "INNER JOIN schedules schedule ON schedule.film_id = film.id "
            + "INNER JOIN seats seat ON seat.schedule_id = schedule.schedule_id "
            + "INNER JOIN users user "
            + "WHERE seat.user_id = :user_id LIMIT 1", nativeQuery = true)
    List<TicketReservation> getTicketReservation(@Param("user_id") long userId);

}
