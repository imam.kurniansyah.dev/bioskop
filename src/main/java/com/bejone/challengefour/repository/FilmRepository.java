package com.bejone.challengefour.repository;

import com.bejone.challengefour.model.FilmSchedule;
import com.bejone.challengefour.entity.Film;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface FilmRepository extends JpaRepository<Film, Long> {
    @Modifying
    @Query(value = "UPDATE films SET name = :new_name WHERE id = :id", nativeQuery = true)
    void updateFilmNameById(@Param("id") long id, @Param("new_name") String newName);

    @Modifying
    @Query(value = "SELECT id, name, is_playing FROM films WHERE is_playing = 1", nativeQuery = true)
    List<Film> getNowPlayingMovie();

    /**
     * Using projection to convert query result into Custom object.
     * @see <a href="https://stackoverflow.com/questions/36328063/how-to-return-a-custom-object-from-a-spring-data-jpa-group-by-query">Stack overflow</a>
      */
    @Modifying
    @Query(value = "SELECT f.name as name, f.is_playing as playing, s.date as date, s.start as start, s.end as end "
            + "FROM films f INNER JOIN schedules s ON s.film_id = f.id "
            + "WHERE s.film_id = :film_id AND f.is_playing = 1", nativeQuery = true)
    List<FilmSchedule> getFilmSchedule(@Param("film_id") long filmId);
}
