package com.bejone.challengefour.repository;

import com.bejone.challengefour.entity.Role;
import com.bejone.challengefour.entity.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    public Role findByUserRole(UserRole userRole);
}
