package com.bejone.challengefour.dto;

import lombok.Data;

@Data
public class RoleDto {
    private String role;
}
