package com.bejone.challengefour.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collection;
import lombok.Data;

@Data
public class UserDto {
    private long id;

    @JsonProperty("user_name")
    private String userName;

    private String email;

    private String password;

    private Collection<RoleDto> roles = new ArrayList<>();
}
