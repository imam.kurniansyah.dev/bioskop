package com.bejone.challengefour.controller;

import com.bejone.challengefour.NetworkResult;
import com.bejone.challengefour.mapper.UserMapper;
import com.bejone.challengefour.request.CreateUser;
import com.bejone.challengefour.request.UpdateUser;
import com.bejone.challengefour.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper mapper;

    @Operation(summary = "Endpoint to create a new user")
    @PostMapping("/create_user")
    NetworkResult<Object> createUser(@RequestBody @Valid CreateUser request) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            userService.createUser(request);
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Create user success");
            response.setData(request);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to update registered user")
    @PutMapping("/update_user")
    NetworkResult<Object> updateUser(@RequestBody @Valid UpdateUser request) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            userService.updateUser(mapper.updateUserToEntity(request));
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Update user success");
            response.setData(request);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to delete registered user")
    @DeleteMapping("/delete_user")
    NetworkResult<Object> deleteUser(@RequestParam long id) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            userService.deleteUser(id);
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Delete user success");
            response.setData(null);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }
}
