package com.bejone.challengefour.controller;

import com.bejone.challengefour.model.TicketReservation;
import com.bejone.challengefour.service.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(
        name = "Controller for Reservation",
        description = "This controller used for getting and processing all information related to Reservation"
)
@RestController
@RequestMapping("/api/v1/reservation")
public class ReservationController {

    @Autowired
    private ReservationService service;

    @Operation(summary = "Endpoint to generate Reservation Ticket by User ID")
    @GetMapping("/create_pdf")
    void createPdfReservation(HttpServletResponse response, @RequestParam("user_id") long userId) throws IOException, JRException {
        JasperReport fileName = JasperCompileManager.compileReport(
                ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "reservation_ticket.jrxml")
                        .getAbsolutePath());
        List<Map<String, String>> dataList = new ArrayList<>();

        List<TicketReservation> ticketReservations = service.getTicketReservation(userId);
        if (ticketReservations.isEmpty()) {
            // dummy data
            Map<String, String> data = new HashMap<>();
            data.put("filmName", "Avenger: The Legend of Aang");
            data.put("userName", "Imam Kurniansyah");
            data.put("studioName", "MNC Studio");
            data.put("date", "13 March 2022");
            data.put("programmeTime",  "13:00 - 15:00");
            data.put("seatNumber", "22");
            dataList.add(data);
        } else {
            ticketReservations.forEach(ticketReservation -> {
                Map<String, String> data = new HashMap<>();
                data.put("filmName", ticketReservation.getFilm());
                data.put("userName", ticketReservation.getUser());
                data.put("studioName", ticketReservation.getStudio());
                data.put("date", ticketReservation.getDate());
                data.put("programmeTime", ticketReservation.getStart() + "-" + ticketReservation.getEnd());
                data.put("seatNumber", String.valueOf(ticketReservation.getSeat()));
                dataList.add(data);
            });
        }

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> signedBy = new HashMap<>();
        signedBy.put("createdBy", "Admin");
        JasperPrint jasperPrint = JasperFillManager.fillReport(fileName, signedBy, dataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=ticket_reservation.pdf;");

        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

    }

}
