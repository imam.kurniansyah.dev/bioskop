package com.bejone.challengefour.controller;

import com.bejone.challengefour.NetworkResult;
import com.bejone.challengefour.model.FilmSchedule;
import com.bejone.challengefour.entity.Film;
import com.bejone.challengefour.mapper.FilmMapper;
import com.bejone.challengefour.request.CreateFilm;
import com.bejone.challengefour.request.UpdateFilmName;
import com.bejone.challengefour.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/film")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private FilmMapper mapper;

    @Operation(summary = "Endpoint to create new film")
    @PostMapping("/create_film")
    NetworkResult<Object> createFilm(@RequestBody @Valid CreateFilm request) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            // TODO: Checking Film Name before saving to avoid duplication
            filmService.createFilm(mapper.createFilmToEntity(request));
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Create film success");
            response.setData(request);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to update registered film")
    @PutMapping("/update_film_name")
    NetworkResult<Object> updateFilmName(@RequestBody @Valid UpdateFilmName request) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            filmService.updateFilmName(request.getId(), request.getNewName());
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Update film name success");
            response.setData(request);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to delete registered film")
    @DeleteMapping("/delete_film")
    NetworkResult<Object> deleteFilm(@RequestParam long id) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            filmService.deleteFilm(id);
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Delete film success");
            response.setData(null);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to get Now Playing film")
    @GetMapping("/now_playing_film")
    NetworkResult<Object> getNowPlayingFilm() {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            List<Film> films = filmService.getNowPlayingFilm();
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Get now playing film success");
            response.setData(films);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }

    @Operation(summary = "Endpoint to get Now Playing film schedule")
    @GetMapping("/now_playing_film_schedule")
    NetworkResult<Object> getFilmSchedule(@RequestParam("film_id") long filmId) {
        NetworkResult<Object> response = new NetworkResult<>();
        try {
            List<FilmSchedule> films = filmService.getFilmSchedule(filmId);
            response.setResponseCode(HttpStatus.OK.value());
            response.setResponseMessage("Get now playing film schedule success");
            response.setData(films);
            return response;
        } catch (Exception e) {
            response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setResponseMessage(e.getLocalizedMessage());
            response.setData(null);
            return response;
        }
    }
}
