package com.bejone.challengefour.service;

import com.bejone.challengefour.model.FilmSchedule;
import com.bejone.challengefour.entity.Film;
import com.bejone.challengefour.repository.FilmRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Override public void createFilm(Film film) {
        filmRepository.save(film);
    }

    @Override public void updateFilmName(long id, String newName) {
        filmRepository.updateFilmNameById(id, newName);
    }

    @Override public void deleteFilm(long id) {
        filmRepository.deleteById(id);
    }

    @Override public List<Film> getNowPlayingFilm() {
        return filmRepository.getNowPlayingMovie();
    }

    @Override public List<FilmSchedule> getFilmSchedule(long filmId) {
        return filmRepository.getFilmSchedule(filmId);
    }
}
