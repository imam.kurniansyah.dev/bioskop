package com.bejone.challengefour.service;

import com.bejone.challengefour.model.TicketReservation;
import com.bejone.challengefour.repository.ReservationRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Override public List<TicketReservation> getTicketReservation(long userId) {
        return reservationRepository.getTicketReservation(userId);
    }
}
