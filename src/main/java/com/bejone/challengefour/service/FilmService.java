package com.bejone.challengefour.service;

import com.bejone.challengefour.model.FilmSchedule;
import com.bejone.challengefour.entity.Film;
import java.util.List;

public interface FilmService {
    void createFilm(Film film);
    void updateFilmName(long id, String newName);
    void deleteFilm(long id);
    List<Film> getNowPlayingFilm();
    List<FilmSchedule> getFilmSchedule(long filmId);
}
