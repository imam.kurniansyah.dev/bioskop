package com.bejone.challengefour.service;

import com.bejone.challengefour.dto.UserDto;
import com.bejone.challengefour.entity.User;
import com.bejone.challengefour.request.CreateUser;

public interface UserService {
    void createUser(CreateUser user);
    void updateUser(User user);
    void deleteUser(long userId);
    UserDto findUserByEmail(String email);
}
