package com.bejone.challengefour.service;

import com.bejone.challengefour.model.TicketReservation;
import java.util.List;

public interface ReservationService {
    List<TicketReservation> getTicketReservation(long userId);
}
