package com.bejone.challengefour.service;

import com.bejone.challengefour.dto.UserDto;
import com.bejone.challengefour.entity.Role;
import com.bejone.challengefour.entity.User;
import com.bejone.challengefour.entity.UserRole;
import com.bejone.challengefour.mapper.UserMapper;
import com.bejone.challengefour.repository.RoleRepository;
import com.bejone.challengefour.repository.UserRepository;
import com.bejone.challengefour.request.CreateUser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override public void createUser(CreateUser user) {
        User existingUser = userRepository.findByEmail(user.getEmail());

        if (existingUser == null) {
            Role userRole = getUserRole(user.getRoleId());
            List<Role> listRole = new ArrayList<>();
            listRole.add(userRole);
            User userEntity = new User();
            userEntity.setUserName(user.getUserName());
            userEntity.setEmail(user.getEmail());
            userEntity.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userEntity.setRoles(listRole);
            userRepository.save(userEntity);
        }
    }

    @Override public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    @Nullable
    @Override public UserDto findUserByEmail(String email) {
        Optional<User> userOptional = Optional.ofNullable(userRepository.findByEmail(email));
        if (userOptional.isPresent()) {
            return userMapper.toUserDto(userOptional.get(), true);
        }
        throw new EntityNotFoundException();
    }

    private Role getUserRole(int roleId) {
        if (roleId == 1) {
            return roleRepository.findByUserRole(UserRole.ADMIN);
        } else {
            return roleRepository.findByUserRole(UserRole.USER);
        }
    }
}
