package com.bejone.challengefour.response;

import lombok.Data;

@Data
public class UserResponse {
    private String email;
    private String token;
}
