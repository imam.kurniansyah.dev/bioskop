package com.bejone.challengefour.mapper;

import com.bejone.challengefour.entity.Film;
import com.bejone.challengefour.request.CreateFilm;
import org.springframework.stereotype.Component;

@Component
public class FilmMapper {
    public Film createFilmToEntity(CreateFilm createFilm) {
        Film film = new Film();
        film.setName(createFilm.getName());
        film.setIsPlaying(createFilm.getIsPlaying());
        return film;
    }
}
