package com.bejone.challengefour.mapper;

import com.bejone.challengefour.dto.RoleDto;
import com.bejone.challengefour.dto.UserDto;
import com.bejone.challengefour.entity.Role;
import com.bejone.challengefour.entity.User;
import com.bejone.challengefour.request.CreateUser;
import com.bejone.challengefour.request.UpdateUser;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User createUserToEntity(CreateUser createUser) {
        User user = new User();
        user.setEmail(createUser.getEmail());
        user.setPassword(createUser.getPassword());
        user.setUserName(createUser.getUserName());
        return user;
    }

    public User updateUserToEntity(UpdateUser updateUser) {
        User user = new User();
        user.setId(updateUser.getId());
        user.setEmail(updateUser.getEmail());
        user.setPassword(updateUser.getPassword());
        user.setUserName(updateUser.getUserName());
        return user;
    }

    public UserDto toUserDto(User user, boolean isShowPassword) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUserName(user.getUserName());
        userDto.setEmail(user.getEmail());
        if (isShowPassword) {
            userDto.setPassword(user.getPassword());
        } else {
            userDto.setPassword("ih kepo");
        }
        userDto.setRoles(convertRoleToDto(user.getRoles()));
        return userDto;
    }

    public Collection<RoleDto> convertRoleToDto(Collection<Role> roles) {
        Collection<RoleDto> roleDtos = new ArrayList<>();
        roles.forEach(role -> {
            RoleDto roleDto = new RoleDto();
            roleDto.setRole(String.valueOf(role.getUserRole()));
            roleDtos.add(roleDto);
        });
        return roleDtos;
    }
}
