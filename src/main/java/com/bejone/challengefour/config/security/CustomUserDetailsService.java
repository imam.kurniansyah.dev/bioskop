package com.bejone.challengefour.config.security;

import com.bejone.challengefour.dto.RoleDto;
import com.bejone.challengefour.dto.UserDto;
import com.bejone.challengefour.service.UserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDto userDto = userService.findUserByEmail(email);
        if (userDto != null) {
            List<GrantedAuthority> authorities = getUserAuthority(userDto.getRoles());
            return buildUserForAuthentication(userDto, authorities);
        }
        throw new UsernameNotFoundException("user with email " + email + "does not exist.");
    }

    private List<GrantedAuthority> getUserAuthority(Collection<RoleDto> userRoles) {
        List<GrantedAuthority> roles = new ArrayList<>();
        userRoles.forEach(roleDto -> roles.add(new SimpleGrantedAuthority(roleDto.getRole())));
        return roles;
    }

    private UserDetails buildUserForAuthentication(UserDto userDto, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(userDto.getEmail(), userDto.getPassword(),
                authorities);
    }
}
